# Qubik analysis

First rough script of Qubik telemetry taken from db-dev.satnogs.org;
IIUC, this is data taken during integration testing (ie, *before*
launch).  Tracking issue in Polaris:
https://gitlab.com/librespacefoundation/polaris/polaris/-/issues/151.

The data has been included here, but can also be downloaded from
db-dev.satnogs.org.  See the Makefile for links.

# Graph files

The graph files produced as part of this analysis are included here as
`initial_results/qubik-1-graph.json` and `initial_results/qubik-2-graph.json`.

# Procedure

Assuming a clean installation of Polaris in a virtualenv:

- In that virtualenv, edit `polaris/fetch/satellites.json` and add
  these stanzas:

```

@@ -238,5 +238,17 @@
     "name": "Quetzal-1",
     "decoder": "quetzal1",
     "normalizer": "Quetzal1"
+  },
+  {
+    "norad_id": "90000",
+    "name": "Qubik-1",
+    "decoder": "Qubik",
+    "normalizer": "Qubik"
+  },
+  {
+    "norad_id": "90001",
+    "name": "Qubik-2",
+    "decoder": "Qubik",
+    "normalizer": "Qubik"
   }
 ]
```

- Edit the Makefile and make sure that the `PVENV` variable points to
  the `activate` file for the virtualenv that contains Polaris.

- Assuming I've got everything right...you *should* be able to run:

```
make qubik-1
make qubik-2
```

  ...to do the import and analysis for the two satellites.

- You should be able to view the graph files locally like so:

```
make view-1
make view-2
```

Graph files for these satellites are also being hosted on our website:

- https://polarisml.space/sat/qubik-1
- https://polarisml.space/sat/qubik-2

You'll probably notice some difference between the two, but it
shouldn't be much.
