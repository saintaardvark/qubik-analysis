#
# Customize these variables:
#
# Path to virtualenv activation file.
PVENV=~/dev/src/polaris-clean/.venv/bin/activate

# These were obtained from
# - https://db-dev.satnogs.org/satellite/99000/
# - https://db-dev.satnogs.org/satellite/99001/
#
# You'll need to log in and select the Data tab, then select the
# option to export all frames.
QUBIK1_FRAMES=./99000-148-20210326T165538Z-all.csv
QUBIK2_FRAMES=./99001-148-20210326T165528Z-all.csv

QUBIK1_NORMALIZED=./qubik-1-normalized_frames.json
QUBIK2_NORMALIZED=./qubik-2-normalized_frames.json

QUBIK1_GRAPH=./qubik-1-graph.json
QUBIK2_GRAPH=./qubik-2-graph.json

$(QUBIK1_FRAMES): $(QUBIK1_FRAMES).gz
	gunzip $(QUBIK1_FRAMES).gz

$(QUBIK2_FRAMES): $(QUBIK2_FRAMES).gz
	gunzip $(QUBIK2_FRAMES).gz

qubik-1: $(QUBIK1_FRAMES) fetch-1 learn-1
qubik-2: $(QUBIK2_FRAMES) fetch-2 learn-2

fetch-1:
	source $(PVENV) && \
	polaris fetch \
		--import_file $(QUBIK1_FRAMES) \
		--skip_normalizer \
		Qubik-1 \
		$(QUBIK1_NORMALIZED)

learn-1:
	source $(PVENV) && \
		time polaris learn \
		-g $(QUBIK1_GRAPH) \
		--force_cpu \
		$(QUBIK1_NORMALIZED)

fetch-2:
	source $(PVENV) && \
	polaris fetch \
		--import_file $(QUBIK2_FRAMES) \
		--skip_normalizer \
		Qubik-2 \
		$(QUBIK2_NORMALIZED)

learn-2:
	source $(PVENV) && \
		time polaris learn \
		-g $(QUBIK2_GRAPH) \
		--force_cpu \
		$(QUBIK2_NORMALIZED)

view-1:
	source $(PVENV) && \
		polaris viz \
			$(QUBIK1_GRAPH)

view-2:
	source $(PVENV) && \
		polaris viz \
			$(QUBIK2_GRAPH)
